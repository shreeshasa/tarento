package com.tarento;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

public class Graph<T> {

  private final Map<T, List<T>> graph = new HashMap<>();

  /**
   * @param node
   *
   * @return
   */
  public boolean addNode(T node) {
    if (node == null) {
      throw new RuntimeException("The input node cannot be null.");
    }
    if (graph.containsKey(node)) {
      return false;
    }
    graph.put(node, new ArrayList<>());
    return true;
  }

  /**
   * @param source
   * @param destination
   */
  public void addEdge(T source, T destination) {
    if (source == null || destination == null) {
      throw new RuntimeException("Source and Destination, both should be non-null.");
    }
    if (!graph.containsKey(source) || !graph.containsKey(destination)) {
      throw new NoSuchElementException("Source and Destination, both should be part of graph");
    }
    graph.get(source).add(destination);
  }

  /**
   * @param node
   *
   * @return
   */
  public List<T> edgesFrom(T node) {
    if (node == null) {
      throw new RuntimeException("The node should not be null.");
    }
    List<T> edges = graph.get(node);
    if (edges == null) {
      throw new NoSuchElementException("Source node does not exist.");
    }
    return Collections.unmodifiableList(edges);
  }

  /**
   * @param source
   * @param destination
   *
   * @return
   */
  public List<List<T>> findAllPaths(T source, T destination) {
    validate(source, destination);

    List<List<T>> paths = new ArrayList<>();
    recursive(source, destination, paths, new LinkedHashSet<>());
    return paths;
  }

  /**
   * @param source
   * @param destination
   */
  private void validate(T source, T destination) {
    if (source == null) {
      throw new RuntimeException("The source cannot be null.");
    }
    if (destination == null) {
      throw new RuntimeException("The destination cannot be null.");
    }
    if (source.equals(destination)) {
      throw new IllegalArgumentException("The source and destination: " + source + " cannot be the same.");
    }
  }

  /**
   * @param current
   * @param destination
   * @param paths
   * @param path
   */
  private void recursive(T current, T destination, List<List<T>> paths, LinkedHashSet<T> path) {
    path.add(current);
    // System.out.println("current: " + current + ", destination: " + destination);
    // System.out.println("path: " + path);
    // System.out.println("paths: " + paths);

    if (current.equals(destination)) {
      paths.add(new ArrayList<>(path));
      // paths.forEach(p -> System.out.println(String.join(" -> ", p.stream().map(i -> i.toString()).collect(Collectors.toList()))));
      path.remove(current);
      return;
    }

    final List<T> edges = edgesFrom(current);
    edges.forEach(t -> recursive(t, destination, paths, path));
    path.remove(current);
  }
}
