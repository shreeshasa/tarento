package com.tarento;

public class Program2 {

  public static void main(String[] args) {
    String s1 = "PPPPPPPPPPPPPPPPPPPP";
    String s2 = "QQQQQQQQQQQQQQQQQQQQ";
    String s3 = "RRRRRRRRRRRRRRRRRRRR";
    String s = concat(s1, s2, s3);
    System.out.println(s);
  }

  private static String concat(String s1, String s2, String s3) {
    if (isEmpty(s1) || isEmpty(s2) || isEmpty(s3)) {
      return "";
    }
    StringBuilder sb = new StringBuilder();
    sb.append(s1.charAt(0)).append(s2.charAt(0)).append(s3.charAt(0));
    return sb.toString() + concat(s1.substring(1), s2.substring(1), s3.substring(1));
  }

  private static boolean isEmpty(String str) {
    return str == null || str.trim().length() == 0;
  }
}
