package com.tarento;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Program1 {

  public static Map<Integer, List<Integer>> graphConfig;

  static {
    graphConfig = new HashMap<>();
    graphConfig.put(1, Arrays.asList(2, 3, 4, 5));
    graphConfig.put(2, Arrays.asList(6));
    graphConfig.put(3, Arrays.asList(6, 7));
    graphConfig.put(4, Arrays.asList(7, 8));
    graphConfig.put(5, Arrays.asList(8));
  }

  public static void main(String[] args) {
    // initialize graph
    Graph<Integer> graph = new Graph<>();
    graphConfig.forEach((s, v) -> {
      graph.addNode(s);
      if (v != null && !v.isEmpty()) {
        v.forEach(d -> {
          graph.addNode(d);
          graph.addEdge(s, d);
        });
      }
    });

    int s = 1;
    for (Integer d : Arrays.asList(6, 7, 8)) {
      List<List<Integer>> paths = graph.findAllPaths(s, d);
      paths.forEach(p -> System.out.println(String.join(" -> ", p.stream().map(i -> i.toString()).collect(Collectors.toList()))));
    }
  }
}
